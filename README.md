# 《软件工程》期末考试

此代码仓库用于2024年《软件工程》课程的补考考试。

## 项目介绍

本项目包含一个简单的石头剪刀布（Rock Paper Scissors）游戏，学生需要在此基础上进行代码编写和重构。

## 诚信承诺书
本人张衷耀，学号2022035111288，承诺遵守学术诚信原则，独立完成考试，不抄袭，不作弊。
