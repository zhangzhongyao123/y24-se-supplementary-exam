import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

/**
 * @author xyxy
 */
public class RockPaperScissors {


    // TODO: 请按照任务书的代码实现此方法

    public static int initializeGame() {


        Scanner scanner = new Scanner(System.in);
        System.out.println("欢迎来到剪刀手头布游戏！");
        System.out.println("请输入你想玩的回合数：");
        return scanner.nextInt();


    }


    // TODO: 请按照任务书的代码实现此方法
    public static String getPlayerMove() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的选择（石头，剪刀或布）:");
        return scanner.nextLine().toLowerCase();

    }


    public static String getComputerMove() {
        String[] moves = {"石头","剪刀","布"};
        Random random = new Random();
        int index = random.nextInt(moves.length);
        return moves[index];

    }

    // TODO: 请按照任务书的代码实现此方法
    public static int determineWinner(String playerMove, String computerMove) {
        if(playerMove.equals(computerMove)){
            return 0;
        }
        switch (playerMove){
            case "石头":
                return (computerMove.equals("剪刀") ? 1 : -1);
            case "剪刀":
                return (computerMove.equals("布")? 1 : -1);
            case "布":
                return (computerMove.equals("石头")? 1: -1);
            default:
                System.out.println("无效的选择! 你输掉了这一局。");

        }
       return -1;
    }

    // TODO: 请按照任务书的代码实现此方法
    public static void displayResult(int playerScore, int computerScore) {
        System.out.println("最终比分 - 玩家:"+playerScore + "电脑:"+computerScore);
        if(playerScore>computerScore){
            System.out.println("恭喜! 你赢得了游戏!");
        }else if(playerScore<computerScore){
            System.out.println("很遗憾，电脑赢得了游戏!");
        }else {
            System.out.println("游戏是平局");
        }

    }

    // TODO: 请按照任务书的代码实现此方法




    public static void main(String[] args) {
        int rounds = initializeGame();
        int playerScore = 0;
        int computerScore = 0;
        for(int i = 0; i<rounds;i++){
            String playerMove = getPlayerMove();
            String computerMove = getComputerMove();
            System.out.println("电脑选择了："+computerMove);
            int result = determineWinner(playerMove,computerMove);
            if(result==1){
                playerScore++;
                System.out.println("你赢了这一句!");
            }
            else if(result==-1){
                computerScore++;
                System.out.println("电脑赢了这一局");
            }
            else {
                System.out.println("这一局是平局");
            }
            System.out.println("当前比分-玩家:"+playerScore+"电脑"+computerScore);
        }
        displayResult(playerScore,computerScore);
        System.out.println("----- 《软件工程》补考 -----");
    }
}

